<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use DB;
use Session;
use Validator;
use Redirect;

class RegisterController extends Controller
{
    
public function store(UserRequest $request)
    {
        $data = array();
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['password'] = Hash::make($request->password);
        $data['remember_token'] = $request->_token;
      
        
        DB::table('users')->insert($data);
        Session::put('s_mgs', 'User Created ! Please Login');
        return redirect('/login');
    }


}
