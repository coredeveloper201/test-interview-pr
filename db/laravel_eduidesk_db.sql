-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2019 at 09:45 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_eduidesk_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `message` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `name`, `email`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Prodip Roy', 'prodip5080@gmail.com', 'Hello test email', '2019-01-04 20:40:27', '2019-01-04 20:40:27'),
(2, 'Abeer Ahmed', 'prodip5080@gmail.com', 'Hello test email 2', '2019-01-04 20:44:35', '2019-01-04 20:44:35');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(2) NOT NULL,
  `email_type` varchar(50) NOT NULL,
  `smtp_username` varchar(100) NOT NULL,
  `smtp_password` varchar(150) NOT NULL,
  `smtp_server` varchar(100) DEFAULT NULL,
  `smtp_port` varchar(20) NOT NULL,
  `smtp_security` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `email_type`, `smtp_username`, `smtp_password`, `smtp_server`, `smtp_port`, `smtp_security`, `created_at`, `updated_at`) VALUES
(1, 'smpt', 'terjoinclub@gmail.com', 'son147258369', 'smtp.gmail.com', '465', 'ssl', '2019-01-04 06:58:48', '2019-01-04 06:58:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(5) NOT NULL,
  `name` varchar(120) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `phone` varchar(14) DEFAULT NULL,
  `remember_token` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'PRODIP ROY', 'prodip5080@gmail.com', '$2y$10$lHaB7IXq93g9KnAsP4wo7OjsQSvtlKSud2W9tCq2Jql0Gqpw96QT2', NULL, 'mwUo1PcHokCMzYqTl77hpL9jFyxQxsXTAzz5R2xu', '2018-12-31 23:10:13', NULL, NULL),
(2, 'Abeer', 'prodiproy02@gmail.com', '$2y$10$7sRbdB2yECE2vz0mhLTc5.BEN4Jwv6Y2NCoUJbMQDcObC4q9jrIoy', NULL, 'v56oYQ6OhLl8oXAop5VwVcc5s6cN8zAYXPtg9d1H', '2019-01-02 23:37:58', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
