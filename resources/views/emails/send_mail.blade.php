<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Email Notice</title>
		<style type="text/css">
			body{width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
			body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table td{border-collapse:collapse;}
			#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}
			#templateContainer{
				 border: 1px solid #BBBBBB;
			}
			h1, .h1{
				 color:#202020;
				display:block;
				font-family:Arial;
				font-size:40px;
                                font-weight:bold;
				line-height:100%;
				margin-top:2%;
				margin-right:0;
				margin-bottom:1%;
				margin-left:0;
				text-align:left;
			}
			h2, .h2{
				color:#404040;
				display:block;
				font-family:Arial;
				font-size:18px;
				font-weight:bold;
				line-height:100%;
				margin-top:2%;
				margin-right:0;
				margin-bottom:1%;
				margin-left:0;
                                text-align:left;
			}

			h3, .h3{
				color:#606060;
				display:block;
				font-family:Arial;
				font-size:16px;
				font-weight:bold;
				line-height:100%;
				margin-top:2%;
				margin-right:0;
				margin-bottom:1%;
				margin-left:0;
				text-align:left;
			}

			
			h4, .h4{
				color:#808080;
				display:block;
				font-family:Arial;
				font-size:14px;
				font-weight:bold;
				line-height:100%;
				margin-top:2%;
				margin-right:0;
				margin-bottom:1%;
				margin-left:0;
				text-align:left;
			}
			
			#templatePreheader{
				background-color:#eeeeee;
			}
			
			
			.preheaderContent div{
				color:#707070;
				font-family:Arial;
				font-size:10px;
				line-height:100%;
				text-align:left;
			}
			
			.preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{
				color:#2bb6a3;
				font-weight:normal;
				text-decoration:underline;
			}
			
			#social div{
				text-align:right;
			}
			
			#templateHeader{
				background-color:#FFFFFF;
				border-bottom:5px solid #505050;
			}
			
			.headerContent{
				color:#202020;
				font-family:Arial;
				font-size:34px;
				font-weight:bold;
				line-height:100%;
				padding:10px;
				text-align:right;
				vertical-align:middle;
			}
			
			
			.headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
				color:#2bb6a3;
				font-weight:normal;
				text-decoration:underline;
			}
			
			#headerImage{
				height:auto;
				max-width:600px !important;
			}
			
			#templateContainer, .bodyContent{
				background-color:#FDFDFD;
			}
			
			.bodyContent div{
				color:#505050;
				font-family:Arial;
				font-size:14px;
				line-height:150%;
				text-align:justify;
			}
			.bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{
				color:#2bb6a3;
				font-weight:normal;
				text-decoration:underline;
			}
			
			.bodyContent img{
				display:inline;
				height:auto;
			}
			
			/*  STANDARD STYLING: SIDEBAR  */
			
			
			#templateSidebar{
				background-color:#FDFDFD;
			}
			
			.sidebarContent{
				border-left:1px solid #DDDDDD;
			}
			
			.sidebarContent div{
				color:#505050;
				font-family:Arial;
				font-size:10px;
				line-height:150%;
				text-align:left;
			}
			
			.sidebarContent div a:link, .sidebarContent div a:visited, /* Yahoo! Mail Override */ .sidebarContent div a .yshortcuts /* Yahoo! Mail Override */{
				color:#2bb6a3;
				font-weight:normal;
				text-decoration:underline;
			}
			
			.sidebarContent img{
				display:inline;
				height:auto;
			}
			
			/*  STANDARD STYLING: FOOTER  */
			
			#templateFooter{
				background-color:#FAFAFA;
				border-top:3px solid #909090;
			}
			
			.footerContent div{
				color:#707070;
				font-family:Arial;
				font-size:11px;
				line-height:125%;
				text-align:left;
			}
			
			.footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{
				color:#2bb6a3;
				font-weight:normal;
				text-decoration:underline;
			}
			
			.footerContent img{
				display:inline;
			}
			
			#social{
				background-color:#FFFFFF;
				border:0;
			}
			
			#social div{
				text-align:left;
			}
			
			#utility{
				background-color:#FAFAFA;
				border-top:0;
			}

			#utility div{
				text-align:left;
			}
			
			#monkeyRewards img{
				max-width:170px !important;
			}
		</style>
	
	</head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">
            	<tr>
                	<td align="center" valign="top">
                    
                    	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                    <!--  Template Header -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                                        <tr>
                                            <td class="headerContent" width="100%" style="padding-left:20px; padding-right:10px;">
                                            	<div>
                                                    <h1>Equidesk</h1>
                                            	</div>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- End Template Header  -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- Template Body  -->
                                	<table border="0" cellpadding="10" cellspacing="0" width="600" id="templateBody">
                                    	<tr>
                                        	<td valign="top" class="bodyContent">
                                                <!--   Standard Content -->
                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top" style="padding-right:0;">
                                                            <div>
                                                            	<h2 class="h2">Hello {{ $name }}</h2>
                                                                <p>Thank You for Contact with us. We have received your message Successfully.
                                                                    We will contact with you very soon.
                                                                </p>
                                                                
                                                            </div>
							</td>
                                                    </tr>
                                                </table>
                                                <!-- End Standard Content -->
                                            </td>
                                        	<!--  Sidebar  -->
                                        	<td valign="top" width="180" id="templateSidebar">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                	<tr>
                                                    	<td valign="top">
                                                            <!--  Standard Content -->
                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%" class="sidebarContent">
                                                                <tr>
                                                                    <td valign="top" style="padding-right:10px;">
                                                                        <div>
                                                                            <strong>Code Base Tutorial</strong>
                                                                            <br />
                                                                             You can watch more code base tutorial in it.
                                                                            <br />
                                                                            <br />
                                                                            <strong>Social Media</strong>
                                                                            <br />
                                                                            You can contact with us with social media.
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!--  End Standard Content  -->
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <!--  End Sidebar  -->
                                        </tr>
                                    </table>
                                    <!--  End Template Body  -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                    <!--  Begin Template Footer  -->
                                	
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>